﻿package 
{
	/*
	The MIT License
	
	Copyright (c) 2009 nanmo
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	
	//away3d
	import away3d.containers.View3D;
	import away3d.core.base.VertexPosition;
	import away3d.core.math.Number3D;
	
	//tweener
	import caurina.transitions.Tweener;

	internal class ExView3D extends View3D
	{
		protected var correctRotX:Number;
		protected var correctRotY:Number;
		protected var correctRotZ:Number;
		protected var initialRotX:Number;
		protected var initialRotY:Number;
		protected var initialRotZ:Number;
		protected var correctZoom:Number;
		protected var initialZoom:Number;
		protected var minimumZoom:Number;
		protected var maximumZoom:Number;
		protected var zoomRate:Number;
		protected var correctDistance:Number;
		protected var deltaBuffer:int;
		protected var centerPosition:Number3D;
		protected var transtime:Number;
		
		public function ExView3D(init:Object = null,extra:Object = null)
		{
			super(init);
			initialRotX = camera.rotationX;
			initialRotY = camera.rotationY;
			initialRotZ = camera.rotationZ;
			setCorrectRot(initialRotX, initialRotY, initialRotZ);
			if ( init == null)
				return;
			if ( init.zoom[0] )
			{
				initialZoom = ( isNaN(init.zoom[1]) ) ? 10 : init.zoom[1];
				maximumZoom = ( isNaN(init.zoom[2]) ) ? 20 : init.zoom[2];
				minimumZoom = ( isNaN(init.zoom[3]) ) ? 1 : init.zoom[3];
				zoomRate = ( isNaN(init.zoom[4]) ) ? 1 : init.zoom[4];
			}
			else
			{
				initialZoom = init.zoom[1];
				minimumZoom = initialZoom;
				maximumZoom = initialZoom;
				zoomRate = 0;
				
			}
			setCorrectZoom( initialZoom );
			correctDistance = ( init.correctDistance == null ) ? 1000 : init.correctDistance;
			centerPosition = init.center;
			transtime = ( isNaN(init.transtime) ) ? 1 : init.transtime;
			camera.focus = ( isNaN(init.focus) ) ? camera.focus : init.focus;
		}
		internal function updateView():void
		{
			camera.position = centerPosition;
			camera.moveBackward(correctDistance);
		}
		internal function addCorrectRot( x:Number=0, y:Number=0, z:Number=0):void
		{
			setCorrectRot(correctRotX + x, correctRotY + y, correctRotZ + z);
		}
		internal function setCorrectRot( x:Number, y:Number, z:Number):void
		{
			correctRotX = x;
			correctRotY = y;
			correctRotZ = z;
			Tweener.addTween(camera, 
				{ rotationX:correctRotX, rotationY:correctRotY, rotationZ:correctRotZ, time:transtime, transition:"easeInOut" } );
		}
		internal function stopRotation():void
		{
			setCorrectRot(camera.rotationX, camera.rotationY, camera.rotationZ);
		}
		internal function addCorrectZoom( zoom:Number):void
		{
			setCorrectZoom(correctZoom + zoom);
		}
		internal function setCorrectZoom( zoom:Number):void
		{
			correctZoom = zoom;
			if (correctZoom < minimumZoom)
			{
				correctZoom = minimumZoom;
			}
			else if (correctZoom > maximumZoom)
			{
				correctZoom = maximumZoom;
			}
			Tweener.addTween( camera, {zoom:correctZoom,transition:"easeInOut",time:transtime} );
		}
		internal function zoomIn():void
		{
			addCorrectZoom(zoomRate);
		}
		internal function zoomOut():void
		{
			addCorrectZoom( -zoomRate);
		}
		internal function mouseWheelZoom(delta:int):void
		{
			if ( evalPlusOrMinus(delta) != evalPlusOrMinus(deltaBuffer) )
			{
				setCorrectZoom(camera.zoom);
			}
			else
			{
				if ( delta > 0 )
				{
					zoomIn();
				}
				else if ( delta < 0 )
				{
					zoomOut();
				}
			}
			deltaBuffer = delta;
		}
		private function evalPlusOrMinus(number:Number):Boolean
		{
			if ( number >= 0 )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		internal function reset():void
		{
			setCorrectRot(initialRotX, initialRotY, initialRotZ);
			setCorrectZoom(initialZoom)
		}
	}
}