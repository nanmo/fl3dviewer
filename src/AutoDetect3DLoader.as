﻿package
{
	/*
	The MIT License
	
	Copyright (c) 2009 nanmo
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	
	import away3d.loaders.*;
	import org.libspark.away3d.Metasequoia;

	public class AutoDetect3DLoader extends Object3DLoader
	{
		
		public function AutoDetect3DLoader(init:Object = null)
		{
			super(init);
		}
		public static function load(url:String, init:Object = null):Object3DLoader
		{
			if ( url.toLowerCase().search(/\.dae$/) != -1 )
			{
				return Collada.load(url, init);
			}
			else if ( url.toLowerCase().search(/\.kmz$/) != -1 )
			{
				return Kmz.load(url, init);
			}
			else if ( url.toLowerCase().search(/\.obj$/) != -1 )
			{
				return Obj.load(url, init);
			}
			else if ( url.toLowerCase().search(/\.3ds$/) != -1 )
			{
				return Max3DS.load(url, init);
			}
			else if ( url.toLowerCase().search(/\.mqo$/) != -1 )
			{
				return Metasequoia.load(url, init);
			}
			else
			{
				return null
			}
			return null;
		}
		
	}
	
}