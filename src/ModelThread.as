﻿package  
{
	/*
	The MIT License
	
	Copyright (c) 2009 nanmo
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	
	import away3d.animators.SkinAnimation;
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.core.base.Mesh;
	import away3d.events.LoaderEvent;
	import away3d.loaders.Object3DLoader;
	import flash.display.Scene;
	import flash.events.Event;
	import org.libspark.thread.Thread;
	
	import flash.utils.getTimer;
	import flash.utils.getDefinitionByName;
	import flash.events.IOErrorEvent;
	
	//Tweener
	import caurina.transitions.Tweener;
	/**
	 * ...
	 * @author nanmo
	 */
	public class ModelThread extends Thread
	{
		private var _url:String;
		private var _init:Object;
		private var _loader:Object3DLoader;
		private var _skinAnimation:SkinAnimation;
		private var _animationSelect:Boolean;
		private var _animationSwitch:Boolean;
		private var _animationTime:uint;
		private var _oldTime:uint;
		private var _scene:Scene3D;
		private var _modifierArray:Array;
		private var _playspeed:Number;
		private var _object:Object;
		public var loadComplete:Boolean;
		
		public function ModelThread( model:Object, scene:Scene3D )
		{
			_object = model;
			_url = model.src;
			_init = { scaling:model.scale, precision:10, 
						x:model.position[0], y:model.position[1], z:model.position[2], 
						rotationX:model.rotation[0], rotationY:model.rotation[1], rotationZ:model.rotation[2]}
			_animationSelect = (model.animation != null) ? model.animation : false;
			_animationSwitch = true;
			_scene = scene;
			_playspeed = ( isNaN(model.playspeed) ) ? 1 : model.playspeed;
			loadComplete = false;
		}
		override protected function run():void
		{
			_loader = AutoDetect3DLoader.load( _url, _init );
			_scene.addChild(_loader);
			event( _loader, LoaderEvent.LOAD_SUCCESS, loadSuccessEvent );
			error( LoaderEvent, loadError, true);
			error( IOErrorEvent, ioError, true);
		}
		private function loadSuccessEvent( event:LoaderEvent ):void
		{
			if ( (_loader.handle.animationLibrary != null) && _animationSelect )
			{
				if (_loader.handle.animationLibrary.getAnimation("default") != null)
				{
					_skinAnimation = ( _loader.handle.animationLibrary.getAnimation("default").animation as SkinAnimation );
				}
				//trace( _loader.handle.animationLibrary.getAnimation("default").animation );
			}
			loadComplete = true;
			next(update);
		}
		private function update():void
		{
			if ( _skinAnimation != null )
			{
				if ( _animationSwitch )
				{
					_animationTime += (getTimer() - _oldTime)*_playspeed;
					_skinAnimation.update( _animationTime/1000 );
				}
				_oldTime = getTimer();
			}
			next(update);
		}
		private function loadError( e:Event, t:Thread):void
		{
			next(null);
		}
		private function ioError( e:Event, t:Thread):void
		{
			trace(e);
			next(null)
		}
		public function set animationSwitch( _switch:Boolean ):void
		{
			_animationSwitch = _switch;
		}
		public function get animationSwitch():Boolean
		{
			return _animationSwitch;
		}
		public function get animationSelect():Boolean
		{
			return _animationSelect;
		}
		public function get loader():Object3DLoader
		{
			return _loader;
		}
		
	}
	
}