﻿package  
{
	/*
	The MIT License
	
	Copyright (c) 2009 nanmo
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	
	//flash
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.display.Sprite;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import flash.errors.IOError;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.text.TextField;
	//import flash.errors.IOError;
	import flash.events.IOErrorEvent;
	
	//away3d
	import away3d.containers.View3D;
	import away3d.core.math.Number3D;
	import away3d.core.render.Renderer;
	import away3d.events.LoaderEvent;
	import away3d.loaders.Object3DLoader;
	import away3d.materials.BitmapFileMaterial;
	import away3d.core.base.Object3D;
	import away3d.loaders.data.AnimationData;
	import away3d.animators.SkinAnimation;
	
	//Thread(soumen)
	import org.libspark.thread.Thread;
	import org.libspark.thread.threads.display.LoaderThread;
	import org.libspark.thread.threads.net.URLLoaderThread;
	import org.libspark.thread.EnterFrameThreadExecutor;
	import org.libspark.thread.utils.ParallelExecutor;
	
	//corelib JSON
	import com.adobe.serialization.json.JSON;
	import com.adobe.serialization.json.JSONParseError;
	
	//Tweener
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.TextShortcuts;
	
	//stats
	import net.hires.debug.Stats;
	
	/**
	 * ...
	 * @author nanmo
	 */
	public class MainThread extends Thread
	{
		private var setting:Object;
		private var jsonFile:String;
		private var jsonLoaderThread:URLLoaderThread;
		private var backgroundLoaderThread:LoaderThread;
		private var _root:Sprite;
		protected var view:ExView3D;
		private var checkMouseDown:Boolean;
		private var oldMouseY:int;
		private var oldMouseX:int;
		private var modelThreads:ParallelExecutor;
		private var backgroundBitmap:Shape;
		
		public function MainThread( r:Sprite)
		{
			_root = r;
		}
		override protected function run():void
		{
			checkMouseDown = false;
			//同一ファイル名の.jsonを読み込む
			jsonFile = _root.loaderInfo.loaderURL.substring( _root.loaderInfo.loaderURL.lastIndexOf("/") + 1, _root.loaderInfo.loaderURL.lastIndexOf(".") )
			jsonFile += ".json";
			jsonLoaderThread = new URLLoaderThread( new URLRequest(jsonFile)  );
			jsonLoaderThread.start();
			jsonLoaderThread.join();
			//event( jsonLoaderThread.loader, IOErrorEvent.IO_ERROR, jsonLoadError);
			error( IOError, jsonLoadError);
			next(jsonParse);
		}
		private function jsonLoadError( e:Error, t:Thread):void
		{
			//trace(e);
			TextShortcuts.init();
			var text:TextField = new TextField();
			text.autoSize = "left";
			text.width = _root.stage.stageWidth;
			text.wordWrap = true;
			_root.addChild(text);
			Tweener.addTween( text, { _text: "設定ファイル" + jsonFile + "の読み込みエラー\n" + e.message, time:1, transition:"linear" } );
			next(null);
		}
		private function jsonParse():void
		{
			//parsing JSON
			try 
			{
				setting = JSON.decode(jsonLoaderThread.loader.data);
				next(jsonCheck);
			}
			catch (e:JSONParseError)
			{
				//trace("えらったー");
				parseError(e);
				//next(parseError(e));
			}
		}
		private function jsonCheck():void
		{			
			//checking JSON
			setting.camera.distance = isNaN(setting.camera.distance) ? 10000 : setting.camera.distance;
			for each( var centerPosition:Number in setting.camera.center )
				centerPosition = isNaN( centerPosition ) ? 0 : centerPosition;
				
			setting.background.tiling = setting.background.tiling is Boolean ? setting.background.tiling : false;
			
			for each( var model:Object in setting.model )
			{
				model.scale = isNaN(model.scale) ? 1 : model.scale;
				for each( var modelPosition:Number in model.position )
					modelPosition = isNaN(modelPosition) ? 0 : modelPosition;
				for each( var modelRotation:Number in model.rotation )
					modelRotation = isNaN(modelRotation) ? 0 : modelRotation;
				
				model.extended = model.extended is Boolean ? model.extended : false;
				model.animation = model.animation is Boolean ? model.animation : false;
			}
			next(setFlashEnvironment);
		}
		private function setFlashEnvironment():void
		{
			_root.stage.frameRate = setting.flash.framerate;
			_root.stage.quality = String(setting.flash.quality);
			if ( setting.flash.stats )
			{
				_root.stage.addChild(new Stats())
			}
			next(setBackgroundColor);
		}
		private function setBackgroundColor():void
		{
			backgroundBitmap = new Shape();
			if ( setting.background.color != null && setting.background.color != "undefined" && setting.background.color.length == 8 )
			{
				backgroundBitmap.graphics.beginFill(uint(setting.background.color));
			}
			else
			{
				backgroundBitmap.graphics.beginFill( 0xFFFFFF );
			}
			backgroundBitmap.graphics.drawRect( 0, 0, _root.stage.stageWidth, _root.stage.stageHeight);
			backgroundBitmap.graphics.endFill();
			next(loadBackgroundImage);
		}
		private function loadBackgroundImage():void
		{
			if ( setting.background.src == null || setting.background.src == "undefined" || setting.background.src.length == 0 )
			{
				_root.stage.addChildAt(backgroundBitmap, 0);
				next(setUpView);
			}
			else
			{
				backgroundLoaderThread = new LoaderThread(new URLRequest(setting.background.src));
				backgroundLoaderThread.start();
				backgroundLoaderThread.join();
				next(setBackgroundImage);
				error(IOError,bgLoadError);
			}
		}
		private function setBackgroundImage():void
		{
			var loader:Loader = backgroundLoaderThread.loader;
			backgroundBitmap.graphics.beginBitmapFill( (loader.content as Bitmap).bitmapData);
			if ( setting.background.tiling )
				backgroundBitmap.graphics.drawRect( 0, 0, _root.stage.stageWidth, _root.stage.stageHeight);
			else
				backgroundBitmap.graphics.drawRect( 0, 0, (loader.content as Bitmap).width, (loader.content as Bitmap).height);
			backgroundBitmap.graphics.endFill();
			
			_root.stage.addChildAt(backgroundBitmap, 0);
			next(setUpView);
		}
		private function setUpView():void
		{
			//setup view
			view = new ExView3D( { x:_root.stage.stageWidth / 2, y:_root.stage.stageHeight / 2, 
				renderer:Renderer.CORRECT_Z_ORDER, transtime:setting.camera.transtime, focus: setting.camera.focus,
				zoom : setting.camera.zoom, correctDistance:setting.camera.distance,
				center:new Number3D(setting.camera.center[0],setting.camera.center[1],setting.camera.center[2]) }  );
			_root.stage.addChild(view);
			
			next(loadModel);
		}
		private function loadModel():void
		{			
			var model:Object;
			var thread:ModelThread;
			modelThreads = new ParallelExecutor();
			
			for each( var modelObj:Object in setting.model )
			{
				modelThreads.addThread( new ModelThread( modelObj , view.scene ) );
			}
			modelThreads.start();
			
			next(renderEvent);
		}
		private function checkModelLoadComplete():Boolean
		{
			for ( var i:int = 0 ; i < modelThreads.numThreads ; i++ )
			{
				if ( !(modelThreads.getThreadAt(i) as ModelThread).loadComplete )
				{
					return false;
				}
			}
			return true;
		}
		private function renderEvent():void
		{
			setEvents();			
			view.updateView();
			view.render();
			
			if ( setting.camera.enterframe || checkMouseDown || !checkModelLoadComplete() )
				next(renderEvent);
		}
		private function setEvents():void
		{
			event( _root.stage, Event.RESIZE, resizeEvent );
			event( _root.stage, MouseEvent.MOUSE_WHEEL, mouseWheelEvent );
			if ( checkMouseDown )
			{
				event( _root.stage, MouseEvent.MOUSE_UP, mouseUpEvent );
				//rotation
				view.addCorrectRot(oldMouseY - _root.stage.mouseY, -(oldMouseX - _root.stage.mouseX)) ;
				oldMouseX = _root.stage.mouseX;
				oldMouseY = _root.stage.mouseY;
			}
			else
			{
				event( _root.stage, MouseEvent.MOUSE_DOWN, mouseDownEvent );
			}
			event( _root.stage, KeyboardEvent.KEY_DOWN, keyDownEvent );
		}
		private function mouseDownEvent(event:MouseEvent):void
		{
			//trace("MouseDown!");
			checkMouseDown = true;
			view.stopRotation();
			oldMouseX = _root.stage.mouseX;
			oldMouseY = _root.stage.mouseY;
			next(renderEvent);
		}
		private function mouseUpEvent(e:MouseEvent):void
		{
			//trace("MouseUp!");
			checkMouseDown = false;
			event( _root.stage, MouseEvent.CLICK, mouseClickEvent);
			event( _root.stage, MouseEvent.DOUBLE_CLICK, doubleClickEvent );
			next(renderEvent);
		}
		private function mouseWheelEvent(event:MouseEvent):void
		{
			view.mouseWheelZoom(event.delta);
			next(renderEvent);
		}
		private function mouseMoveEvent(event:MouseEvent):void
		{
			if (checkMouseDown)
			{
				view.addCorrectRot(oldMouseY - _root.stage.mouseY,-(oldMouseX - _root.stage.mouseX)) ;
			}
			oldMouseX = _root.stage.mouseX;
			oldMouseY = _root.stage.mouseY;
			next(renderEvent);
		}
		private function doubleClickEvent(event:MouseEvent):void
		{
			//trace("doubleClick!");
			for ( var i:uint; i < modelThreads.numThreads ; ++i )
			{
				(modelThreads.getThreadAt(i) as ModelThread).animationSwitch = !(modelThreads.getThreadAt(i) as ModelThread).animationSwitch
			}
			next(renderEvent);
		}
		private function mouseClickEvent(event:MouseEvent):void
		{
			//trace("click!");
			next(renderEvent);
		}
		private function keyDownEvent(event:KeyboardEvent):void
		{
			//if (event.keyCode == 32)
			{
				view.reset();
			}
			next(renderEvent);
		}
		private function resizeEvent(event:Event):void
		{
			view.x = _root.stage.stageWidth / 2;
			view.y = _root.stage.stageHeight / 2;
			next(renderEvent);
		}
		private function parseError( e:Error ):void
		{
			//trace(e.message);
			TextShortcuts.init();
			var text:TextField = new TextField();
			text.autoSize = "left";
			_root.addChild(text);
			Tweener.addTween( text, { _text: "設定ファイル"+ jsonFile + "のパースエラー\n" + e.message, time:1, transition:"linear" } );
			next(null);
		}
		private function bgLoadError( e:Error, t:Thread):void
		{
			//trace("bg load error");
			next(loadModel);
		}
	}
}