FL3DViewer - Flashで3Dモデルビューワ
version.1

Flashで3Dモデルを表示します。
Flashならなんとかなるのでいろんな人向けに何とかなります。

・Flashプレイヤーのバージョン
	9以降で動作します。PS3でも動いたりします、重いけど。

・操作方法
	左クリック・ドラッグ：つかむ、まわす
	ホイール：ズーム調整
	ダブルクリック：（アニメーションがあるとき）モーション停止・再生
	なんかキーを押す：カメラリセット

・対応しているファイル
	モデルファイル　mqo,dae,obj(+mtl)
	テクスチャ　png,jpg
	読めるが変なモデルファイル　3ds,kmz

・BlenderからDAEを書き出す方法
	とりあえずアニメーション無しのものについては
		http://d.hatena.ne.jp/nanmo/20081119/1227104830
	にまとめてあります。
	疑問点などがあったらコメントやメールでお問い合わせください。

・使い方
	htmlにFlash用のObjectタグとEmbedタグを書く　→　設定ファイル（*.json）を書く
	htmlと設定ファイルのひな形をつけてあるのでそこからいじると簡単です。
	Flashの仕様上オンライン用とオフライン用でSWFを変える必要があるため、サンプルはそれぞれフォルダを分けてあります。

・設定ファイル
	設定ファイルのファイル名は、SWFの拡張子をjsonに変えたものになります。
	（例：FL3DViewer.swf -> FL3DViewer.json）
	いわゆる、JSONと呼ばれるデータ形式です。カンマ(,)の位置とか数とか結構うるさくチェックします。

	それぞれのカテゴリに分けて記述します。

	flash - Flashの動作に関する設定
		framerate - 一秒当たりのフレーム数
		quality - 描画品質　"low","medium","high"のいずれか
		stats - 実際のフレーム数やメモリ使用量などを表示するStatsを表示するか(true or false)

	background - 背景に関する設定
		color - 背景色(0x??????)
		src - 背景画像ファイル（jpg,png）
		tiling - 背景画像をタイリング（敷き詰める）か否か(true or false)

	camera - 3Dカメラ設定
		distance - カメラの中心からの距離
		center - カメラの中心座標([x,y,z])
		zoom - ズーム設定、配列になっています
			[ズーム可否(true or false), ズーム初期値, ズーム最大値, ズーム最小値, ズーム変化量]
			ズーム可否がfalseの場合初期値以外は省略可能
		transtime - カメラ動作(回転、ズーム)にかかる秒数
		enterframe - 毎フレーム描画するか否か(true or false)、falseにするとマウスで操作しているときだけ描画します
		focus - カメラの画角（視野角）？（小さいとパースがきつくなる）

	model - 3Dモデル設定(modelは特殊で、配列([])を使ってモデル毎に定義していきます)
		src - 3Dモデルファイル名
		scale - 拡大縮小率（適正な数値は形式、モデル毎に大きく変わります）
		position - モデルの位置([x,y,z])
		rotation - モデルの回転([x,y,z])
		extratex - テクスチャを別に読み込みたいときにテクスチャファイル名を記述します
		animation - Colladaのボーンアニメーションを再生するか否か(true or false)
		playspeed - ボーンアニメーションの再生速度（1で等速）

・ボーンアニメーションの再生について
	確実な書き出し方法などが確立できていないので、アニメーションの再生を保証するものではありません。
	プログラム的には対応させていますが、今のところはおまけ機能扱いです。
	
・マウスホイールに関して
	マウスホイールでズーム操作を行う際に、ブラウザがスクロールしてしまう問題を解決するためにSWFWheelというライブラリを利用しています。
	しかし、SWFWheelはほとんどの環境でローカル動作させることができないので、ローカルではブラウザがスクロールしてしまいます。
	ローカルで実行させることが前提の場合は、zoomをfalseにすることをおすすめします。

・よくあるうっかりミス
	設定ファイルの最後の要素の後には","を入れてはならないのですが、よく消し忘れます。

・わかっている不具合
	回転していると一瞬モデルが消えたりする
		→　その瞬間エラーが発生しているようです。使っているライブラリのバグなので、対応待ちです。
	ズームしたら止まった
		→　あまりに近すぎたり、ズームしすぎたりするとエラーで停止するようです。
	テクスチャを張っているはずなのに真っ白
		→　白くなっている箇所は、UV平面上で面積が0になっています、少しずらしてやると直ります。
	重くね？
		→　正直Flashの3Dエンジンは発展途上なので、現時点でがっつりハイポリを表示するのは難しいです
		　　ちなみに、FirefoxよりIEの方が高速に動作するみたいです。
	陰影つかなくね？
		→　今のところ仕様です。メタセコで自己照明1にするようなモデルを表示するの向け

・どうやって作ってるの？
	FlexSDK + Flashdevelop（どっちも無料）でちまちま作っています。
	さすがに全部一からはやってられないので、いろんなライブラリを利用させてもらっています。
	現時点で
		Away3D(3Dエンジン)
		Thread(Flashで疑似スレッド)
		Metasequoia for Away3D(Away3DでMQOを読む)
		corelib(JSONの読み込みに使ってます)
		Stats(FPSや使用メモリ量を表示)
		Tweener(ぬるぬるうごく)
		SWFWheel(マウスホイール問題の解決)
	を利用しています。

・再配布の際の注意点
	再配布する際にはlicenseフォルダを同梱するようお願いします。

・今後の予定
	Metasequoiaファイルのシェーディング対応
	Cast3D形式の対応
	Colladaのエクスポート法がもっとまとまるといい
	Zip内ファイルの読み込み
	Tweenerによる簡易アニメーション機能
	Simpleshadowによる簡易的な影落とし

author:nanmo
website:http://bleble.s321.xrea.com/
blog:http://d.hatena.ne.jp/nanmo/